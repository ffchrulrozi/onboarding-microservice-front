import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _73ecb5a8 = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages/login" */))
const _4dff9d39 = () => interopDefault(import('..\\pages\\mahasiswa.vue' /* webpackChunkName: "pages/mahasiswa" */))
const _66829c60 = () => interopDefault(import('..\\pages\\matakuliah.vue' /* webpackChunkName: "pages/matakuliah" */))
const _072caff8 = () => interopDefault(import('..\\pages\\pmb.vue' /* webpackChunkName: "pages/pmb" */))
const _435aa048 = () => interopDefault(import('..\\pages\\user.vue' /* webpackChunkName: "pages/user" */))
const _43722a4e = () => interopDefault(import('..\\pages\\watchasdasd.vue' /* webpackChunkName: "pages/watchasdasd" */))
const _ab75dade = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/login",
    component: _73ecb5a8,
    name: "login"
  }, {
    path: "/mahasiswa",
    component: _4dff9d39,
    name: "mahasiswa"
  }, {
    path: "/matakuliah",
    component: _66829c60,
    name: "matakuliah"
  }, {
    path: "/pmb",
    component: _072caff8,
    name: "pmb"
  }, {
    path: "/user",
    component: _435aa048,
    name: "user"
  }, {
    path: "/watchasdasd",
    component: _43722a4e,
    name: "watchasdasd"
  }, {
    path: "/",
    component: _ab75dade,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
