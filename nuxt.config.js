// nuxt.config.js
export default {
    modules: ['@nuxtjs/vuetify'],
    plugins: ['~/plugins/vuetify.js'],
}